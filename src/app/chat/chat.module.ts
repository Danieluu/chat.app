import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ChatComponent } from "./chat.component";
import { ChatService } from "./chat.service";

const routes = [
    { path: '', component: ChatComponent }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    declarations: [ChatComponent],
    providers: [ChatService]
})
export class ChatModule {}