import { Component } from "@angular/core";
import { ChatService, EventName } from "./chat.service";
import { flatMap } from "rxjs/operators";

@Component({
    selector: 'chat',
    templateUrl: './chat.html'
})
export class ChatComponent {
    message: string = '';
    messages: string[] = [];
    roomId: string = '';

    constructor(
        private chatSvc: ChatService
    ) {
        this.chatSvc.connect();
    }

    ngOnInit() { }

    joinRoom() {
        this.chatSvc.sendEvent(EventName.joinRoom);
        this.chatSvc.listenEvent<string>(EventName.joinRoom).pipe(
            flatMap(roomId => {
                this.roomId = roomId;
                return this.chatSvc.listenEvent<string>(this.roomId)
            })
        ).subscribe(result => {
            this.messages.push(result);
        });
    }

    sendMessage() {
        this.chatSvc.sendEvent<{ room: string, text: string }>(EventName.newMessage, { room: this.roomId, text: this.message });
        this.message = '';
    }
}