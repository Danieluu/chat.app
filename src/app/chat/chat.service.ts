import { Injectable } from "@angular/core";
import { fromEvent } from "rxjs";
import { io, Socket } from 'socket.io-client';

@Injectable()
export class ChatService {
    private socket: Socket;

    constructor() { }

    connect() {
        this.socket = io('http://localhost:3000');
        this.socket.connect();
    }

    listenEvent<T>(eventName: EventName | string) {
        return fromEvent<T>(this.socket, eventName);
        // return new Observable<T>(subscribe => {
        //     this.socket.on(eventName, (result: T) => subscribe.next(result));
        // })
    }

    sendEvent<T>(eventName: EventName, options?: T) {
        this.socket.emit(eventName, options);
    }
}

export enum EventName {
    joinRoom = 'joinRoom',
    newMessage = 'newMessage',
    leaveRoom = 'leaveRoom'
}