import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule } from "../common/layout/layout.module";
import { WelcomeComponent } from "./welcome.component";

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: WelcomeComponent }]),
        CommonModule,
        FormsModule,
        LayoutModule
    ],
    declarations: [WelcomeComponent]
})
export class WelcomeModule { }