import { CommonModule } from "@angular/common";
import { Component, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

@Component({
    selector: 'layout',
    templateUrl: './layout.html'
})
export class LayoutComponent { }


@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [LayoutComponent],
    exports: [LayoutComponent]
})
export class LayoutModule { }